#!/usr/bin/make -f
# -*- makefile -*-
# Build rules for the Debian package kalign

include /usr/share/dpkg/default.mk
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND+=-DSIMDE_ENABLE_OPENMP -fopenmp-simd -O3
export DEB_CXXFLAGS_MAINT_APPEND+=-DSIMDE_ENABLE_OPENMP -fopenmp-simd -O3
OBJ_DIR=obj-$(DEB_HOST_GNU_TYPE)
prefix=$(CURDIR)/debian/$(DEB_SOURCE)/usr
libexecdir=$(prefix)/lib/$(DEB_SOURCE)

%:
	dh $@

AMD64_SIMD_LEVELS=avx2 avx sse4.1 ssse3 sse3
i386_SIMD_LEVELS=sse2 sse

override_dh_auto_configure:
	dh_auto_configure -- -DBUILD_SHARED_LIBS=OFF -DINSTALL_PREFIX=/usr -DENABLE_SSE=OFF -DENABLE_AVX=OFF -DENABLE_AVX2=OFF
ifeq (amd64,$(DEB_HOST_ARCH))
	dh_auto_configure --builddirectory obj-sse4.1 -- -DBUILD_SHARED_LIBS=OFF -DINSTALL_PREFIX=/usr -DENABLE_SSE=ON -DENABLE_AVX=OFF -DENABLE_AVX2=OFF
	dh_auto_configure --builddirectory obj-avx -- -DBUILD_SHARED_LIBS=OFF -DINSTALL_PREFIX=/usr -DENABLE_SSE=ON -DENABLE_AVX=ON -DENABLE_AVX2=OFF
	dh_auto_configure --builddirectory obj-avx2 -- -DBUILD_SHARED_LIBS=OFF -DINSTALL_PREFIX=/usr -DENABLE_SSE=ON -DENABLE_AVX=ON -DENABLE_AVX2=ON
endif

ifeq (amd64,$(DEB_HOST_ARCH))
execute_after_dh_auto_build:
	dh_auto_build --builddirectory obj-sse4.1
	dh_auto_build --builddirectory obj-avx
	dh_auto_build --builddirectory obj-avx2
endif

execute_after_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifeq (amd64,$(DEB_HOST_ARCH))
	set -e ; for SIMD in avx2 avx sse4.1 ; do \
		if lscpu | grep -q $${SIMD} ; then \
			cd "obj-$${SIMD}/" && \
			ctest --verbose ; \
			cd .. ; \
		fi ; \
	done
endif
endif

execute_after_dh_install:
	rm -rf debian/$(DEB_SOURCE)/usr/lib/$(DEB_HOST_MULTIARCH)/
	dh_install obj-$(DEB_HOST_GNU_TYPE)/src/kalignfmt usr/bin
ifeq (amd64,$(DEB_HOST_ARCH))
	mkdir -p debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/bin
	cp obj-avx2/src/kalign debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalign-avx2
	cp obj-avx/src/kalign debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalign-avx
	cp obj-sse4.1/src/kalign debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalign-sse4.1
	cp obj-avx2/src/kalignfmt debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalignfmt-avx2
	cp obj-avx/src/kalignfmt debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalignfmt-avx
	cp obj-sse4.1/src/kalignfmt debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalignfmt-sse4.1
	mv debian/$(DEB_SOURCE)/usr/bin/kalign debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalign-plain
	mv debian/$(DEB_SOURCE)/usr/bin/kalignfmt debian/$(DEB_SOURCE)/usr/lib/$(DEB_SOURCE)/kalignfmt-plain
	dh_install -pkalign debian/bin/simd-dispatch /usr/lib/$(DEB_SOURCE)/bin
	cd $(CURDIR)/debian/$(DEB_SOURCE)/usr/bin && \
		ln -s ../lib/$(DEB_SOURCE)/bin/simd-dispatch kalign
	cd $(CURDIR)/debian/$(DEB_SOURCE)/usr/bin && \
		ln -s ../lib/$(DEB_SOURCE)/bin/simd-dispatch kalignfmt
endif

override_dh_gencontrol:
	dh_gencontrol -- -Vsimde:Built-Using="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W "libsimde-dev")"
